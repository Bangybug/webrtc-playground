import {urlOnlineUsers, urlRegisterUser, urlServerInfo} from '../../config'

export const userRegistered = (id,name) => ({ type: 'USER_REGISTERED', payload:{id,name} })
export const onlineUsersLoaded = onlineUsers => ({type: 'USERS_LOADED', onlineUsers })

export function register() {
  return function(dispatch) {
    return fetch(urlServerInfo)
      .then(response => response.json())
      .then(json => {
        // our poor server may be restarted, thus we have to re-register 
        if (json.serverTimestamp !== localStorage.getItem('serverTimestamp')) {
          const serverTimestamp = json.serverTimestamp;
          return fetch(urlRegisterUser)
            .then(response => response.json())
            .then(json => {
              localStorage.setItem('serverTimestamp', serverTimestamp)
              localStorage.setItem('id', json.id);
              localStorage.setItem('name', json.name);
            })
        }
      })
      .then(() => {
        let uid = localStorage.getItem('id');
        if (uid) {
          uid = Number(uid)
        }
        if (typeof uid === 'number') {
          dispatch(userRegistered(uid, localStorage.getItem('name')));
        }
      });
  };
};


export function getOnlineUsers() {
  return function(dispatch) {
    return fetch(urlOnlineUsers)
      .then(response => response.json())
      .then(json => {
        dispatch(onlineUsersLoaded(json));
      });
  };
};

