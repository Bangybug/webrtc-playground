export const userClicked = (user) => ({ type: 'USER_CLICKED', user })

export function onUserClicked(user) {
  return function(dispatch) {
    dispatch(userClicked(user))
  };
};

