import {wsBackend} from '../../config'

export const wsConnect = host => ({ type: 'WS_CONNECT', host });
export const wsConnecting = host => ({ type: 'WS_CONNECTING', host });
export const wsConnected = host => ({ type: 'WS_CONNECTED', host });
export const wsDisconnect = host => ({ type: 'WS_DISCONNECT', host });
export const wsDisconnected = (host,code) => ({ type: 'WS_DISCONNECTED', host, code });

// all what's sent with wsSend is retranslated by go-messenger to all users
export const wsSend = json => ({ type: 'WS_SEND', json });

export function startWebsocket(userid) {
  return function(dispatch) {
    dispatch(wsConnect(wsBackend + userid))
  }
}