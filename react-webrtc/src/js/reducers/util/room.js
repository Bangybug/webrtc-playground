import { STREAMTYPE_REMOTE, STREAMTYPE_DISPLAY, TRACKKIND_SCREEN, TRACKKIND_AUDIO, TRACKKIND_VIDEO } from "../../actions/translation";

export const NewRoom = ({roomUid, name, media, users, p2p, callInProgress, callerId, callerName}) => {
  const room = { 
    roomUid, name, media, users, p2p, callInProgress,
    callerId, callerName,
    userStreams: [], 
    userState: {}
  };
  return room;
}

// For debugging point for all modifications in one place.
export const SetInRoom = (room, props) => {
  for (const key of Object.keys(props)) {
    if (room.hasOwnProperty(key)) {
      room[key] = props[key]
    } 
  }

  // for now, force immutability 
  return {...room}
}

export const USERSTATE_INVITED = 'invited'
export const USERSTATE_WAITING = 'waiting'
export const USERSTATE_CONNECTED = 'connected'
export const USERSTATE_DECLINED = 'declined'
export const USERSTATE_HANGUP = 'hangup'
export const USERSTATE_KICKED = 'kicked'

export const updateUserCallState = (room, invitedUsers, userState) => {
  const roomUserState = (room && room.userState) ? room.userState : {}
  let isChanged = false
  for (const u of invitedUsers) {
    if (!u) { continue }
    isChanged = !isChanged && roomUserState[u.id] !== userState
    roomUserState[u.id] = userState
  }
  return isChanged ? {...roomUserState} : roomUserState
}

export const updateRoomUsers = (room, onlineUsers, updatedUserState) => {
  let roomUsersChanged 
  for (const uid in updatedUserState) {
    const newUser = onlineUsers.find( u => u.id === Number(uid))
    if (newUser && !userInRoom(newUser.id, room)) {
      roomUsersChanged = roomUsersChanged || [...room.users]
      roomUsersChanged.push(newUser)
    }
  }
  return roomUsersChanged || room.users
}

export const activeRoomCanBeUpdatedInState = (room, state) => {
  const ret = {}
  if (state.currentRoom && state.currentRoom.roomUid === room.roomUid && state.currentRoom !== room) {
    ret.currentRoom = room
  }
  const activeRoomOld = state.activeRooms.find( r => r.roomUid === room.roomUid)
  if (activeRoomOld && activeRoomOld !== room) {
    const id = state.activeRooms.indexOf(activeRoomOld)
    if (id !== -1) {
      state.activeRooms[id] = room; 
    }
  }

  return ret
}

export const updateUserStream = (room, {fromUserId, callType, stream, streamType, isRemoved}) => {
  const userStreams = room.userStreams || [];
  if (isRemoved) {
    for (const userStream of userStreams) {
      if (userStream.streams && userStream.streams[stream.id]) {
        delete userStream.streams[stream.id]
      }
      if (userStream.userMediaStreams && userStream.userMediaStreams[stream.id]) {
        delete userStream.userMediaStreams[stream.id]
      }
      if (userStream.displayMediaStreams && userStream.displayMediaStreams[stream.id]) {
        delete userStream.displayMediaStreams[stream.id]
      }
    }
  } else if (streamType === STREAMTYPE_REMOTE) {
    let userStream = userStreams.find( s => s.fromUserId === fromUserId )
    if (!userStream) {
      userStream = {
        fromUserId: fromUserId,
        streams: {},
        callType: callType
      };
      userStreams.push(userStream)
    }
    if (stream) {
      userStream.streams[stream.id] = stream
    }
  } else {
    let userStream = userStreams.find( s => s.local );
    if (!userStream) {
      userStream = {
        local: true,
        userMediaStreams: {},
        displayMediaStreams: {},
        callType: callType
      };
      userStreams.push(userStream);
    }
    const streams = (streamType === STREAMTYPE_DISPLAY) ? userStream.displayMediaStreams : userStream.userMediaStreams
    if (stream) {
      streams[stream.id] = stream
    }
  }
 
  return userStreams;
}


export function findLocalStreamAndTrack(trackKind, userStream) {
  if (userStream) {
    const streams = trackKind === TRACKKIND_SCREEN ? userStream.displayMediaStreams : userStream.userMediaStreams
    for (const streamId in streams) {
      const localStream = streams[streamId]
      const streamTracks = localStream.getTracks()
      for (const track of streamTracks) { 
        if (track.readyState === "ended") { continue }

        if ((track.kind === 'audio' && trackKind === TRACKKIND_AUDIO) || (track.kind === 'video' && trackKind === TRACKKIND_VIDEO)
        || (TRACKKIND_SCREEN && track.kind === 'video')) {
          return {track, trackStream: localStream}
        }
      }
    }
  }
}

// Since I haven't coded backend room support, I need to reference rooms the same way on callee and caller client systems. Lets use callerId and callerRoomId as a 
// room unique identifier (uid). 
export function getRoomUid({callerId, callerRoomId}) { 
  return callerRoomId + ' - ' + callerId 
}

export const findRoom = ({roomUid, activeRooms}) => activeRooms.find( r => r.roomUid === roomUid );

export const userInRoom = (userId, room) => {
  return (room.users || []).find(u => u.id === userId)
}

export function usersInRoomDedupMe( {onlineUsers, userId, callerId, room} ) {
  // this is just for returning users without possible duplicates in case of a self-call
  return room.users
    .filter( u => u.id !== userId ) // filter me out from the room
    .concat(onlineUsers.find( u => u.id === callerId)) // if I'm a caller, then add me, thus I'm in a room
}


export function usersByIdDedupMe( {onlineUsers, userId, callerId, calleeIds} ) {
  // this is just for returning users without possible duplicates in case of a self-call
  return calleeIds
    .filter( cid => cid !== userId ) // filter me out from the room
    .map( cid => onlineUsers.find( u => u.id === cid ))
    .concat(onlineUsers.find( u => u.id === callerId)) // if I'm a caller, then add me, thus I'm in a room
}


export const findRemoteStream = (room, user) => {
  return room.userStreams && room.userStreams.find( us => us.fromUserId === user.id )
}

export const findLocalStream = (userStreams) => {
  return userStreams && userStreams.find( us => us.local )
}

export const getUserState = (room, user) => {
  const us = room.userState && room.userState[user.id];
  return {state: us}
}
