import {auth} from './auth'
import {ui} from './ui'
import {messenger} from './messenger'
import {calls} from './calls'

const initialState = {
  auth: {
    registered: false,  
    me: {},
    loggedIn: false,
  },

  ui: {
    userClicked: null,
  },

  messenger: {
    onlineUsers: []
  },

  calls: {
    supportedTracks: {/*video:true, audio:true*/},
    activeRooms: [],
    currentRoom: null,
    incomingCallInvitation: null
  }

};

// good reading about redux and ways of organizing businesslogic https://medium.com/@jeffbski/where-do-i-put-my-business-logic-in-a-react-redux-application-9253ef91ce1

// it's also useful to have some ideas on redux and websocket with reconnection, I've looked at 
// https://dev.to/aduranil/how-to-use-websockets-with-redux-a-step-by-step-guide-to-writing-understanding-connecting-socket-middleware-to-your-project-km3

export default function rootReducer(state = initialState, action) {
  return {
    auth: auth(state.auth, action),
    ui: ui(state.ui, action),
    messenger: messenger(state.messenger, action, state.auth),
    calls: calls(state.calls, action, state.auth, state.messenger)
  }
};
