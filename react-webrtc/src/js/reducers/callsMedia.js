import {updateUserStream, USERSTATE_CONNECTED, findRoom, activeRoomCanBeUpdatedInState, userInRoom, SetInRoom, updateUserCallState} from './util/room'
import { CALL_STREAM_UPDATED, AVAILABLE_TRACKS_INFO } from '../actions/translation'


export const callsMedia = (state = {}, action/*, authState, messengerState*/) => {
  switch (action.type) {

    
    case AVAILABLE_TRACKS_INFO: {
      if (state.supportedTracks.video !== action.constraints.video || state.supportedTracks.audio !== action.constraints.audio) {
        return {
          ...state,
          supportedTracks: action.constraints
        }
      }
      break;
    }

    case CALL_STREAM_UPDATED: {
      const room = findRoom({ roomUid: action.roomUid, activeRooms: state.activeRooms})
      
      if (room) {
        const updatedRoom = SetInRoom(room, {
          userStreams: updateUserStream(room, action),
          userState: updateUserCallState(room, [userInRoom(action.fromUserId,room)], USERSTATE_CONNECTED)
        })
        
        return {
          ...state,
          ...activeRoomCanBeUpdatedInState(updatedRoom, state)
        }
      }
    
      break;
    }

    default: 
      break;
  }
  return state
}