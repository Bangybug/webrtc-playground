import {ROOM_INIT, CALL_MAKE, SWITCH_ROOM, CALL_HANGUP, CALL_JOIN, CALL_INVITE, CALL_INVITE_PENDING, CALL_DECLINE, CALL_KICK} from '../actions/translation'
import {usersByIdDedupMe, updateUserCallState, USERSTATE_INVITED, USERSTATE_WAITING, userInRoom, USERSTATE_DECLINED, USERSTATE_HANGUP, USERSTATE_KICKED} from './util/room'
import {NewRoom, SetInRoom, findRoom, getRoomUid, activeRoomCanBeUpdatedInState, updateRoomUsers} from './util/room'
import {callsMedia} from './callsMedia'

let roomCounter = 0;

export const calls = (state = {}, action, authState, messengerState) => {
  const myId = authState.me.id
  const myName = authState.me.name

  switch (action.type) {

    case ROOM_INIT: 
      ++roomCounter;
      return {
        ...state,
        currentRoom: NewRoom({ 
          p2p: action.p2p,
          media: action.media,
          users: action.users || [],
          name: action.name,
          roomUid: getRoomUid({callerId: myId, callerRoomId: roomCounter}),
        })
      }
    case SWITCH_ROOM:
      return {
        ...state,
        currentRoom: action.room
      }
    case CALL_MAKE: {
      const currentRoom = SetInRoom(action.room, {
        callInProgress: true,
        callerId: myId,
        callerName: myName,
        userState: updateUserCallState(action.room, action.room.users, USERSTATE_INVITED)
      })
      return {
        ...state,
        activeRooms: [currentRoom].concat(state.activeRooms),
        currentRoom
      }
    }
    case CALL_INVITE: {
      const room = findRoom({roomUid: action.roomUid, activeRooms:state.activeRooms})
      if (room) {
        const updatedUserState = updateUserCallState(room, action.calleeIds.map( cid => messengerState.onlineUsers.find( u => u.id === cid )), USERSTATE_INVITED)
        if (updatedUserState !== room.userState) {

          const updatedUsers = updateRoomUsers(room, messengerState.onlineUsers, updatedUserState)
          const updatedRoom = SetInRoom(room, {
            userState: updatedUserState,
            users: updatedUsers
          })
          return {
            ...state,
            ...activeRoomCanBeUpdatedInState(updatedRoom, state)
          }
        }
      }
      break;
    }
    case CALL_INVITE_PENDING: {
      return {
        ...state,
        incomingCallInvitation: action
      }
    }
    case CALL_DECLINE: {
      // I declined a call?
      if (action.fromUserId === myId && action.toUserId !== myId) {
        return {
          ...state,
          incomingCallInvitation: null
        }
      } else if (action.toUserId === myId) {
        // my call was declined? 
        const room = findRoom({ roomUid: action.roomUid, activeRooms: state.activeRooms})
        if (room) {
          const updatedRoom = SetInRoom(room, {
            userState: updateUserCallState(room, [userInRoom(action.fromUserId,room)], USERSTATE_DECLINED)
          })
          return {
            ...state,
            incomingCallInvitation: null,
            ...activeRoomCanBeUpdatedInState(updatedRoom, state)
          }
        }
      }
      break;
    }

    case CALL_HANGUP:
      const room = findRoom({ roomUid: action.roomUid, activeRooms: state.activeRooms})
      if (room) {
        if (action.fromUserId !== myId) {
          // - I'm in that room and someone just hung up - update that user state in the room
          const updatedRoom = SetInRoom(room, {
            userState: updateUserCallState(room, [userInRoom(action.fromUserId, room)], USERSTATE_HANGUP )
          })
          return {
            ...state,
            ...activeRoomCanBeUpdatedInState(updatedRoom, state)
          }
        } else {
          // - I just hung up - then close the room
          return {
            ...state,
            currentRoom: (room === state.currentRoom) ? null : state.currentRoom,
            activeRooms: state.activeRooms.filter( r => r.roomUid !== action.roomUid )
          }
        }
      }
      break;

    case CALL_JOIN: {
      // is call related to user?
      if (action.calleeIds.indexOf(myId) !== -1) {
        // in case it is a self-call, everything was already setup in makeCall and the room is already created
        // othterwise should create a room for the call
        if (action.callerId !== myId) {
          const roomUsers = usersByIdDedupMe({onlineUsers: messengerState.onlineUsers, userId: myId, callerId: action.callerId, calleeIds: action.calleeIds })
          const newRoom = NewRoom({
            callerId: action.callerId,
            callerName: action.callerName,
            roomUid: action.roomUid,
            p2p: action.p2p,
            name: action.callerRoomName,
            media: action.media,
            users: roomUsers,
            userState: updateUserCallState(null, roomUsers, USERSTATE_WAITING),
            callInProgress: true
          })

          return {
            ...state,
            activeRooms: [newRoom].concat(state.activeRooms),
            currentRoom: newRoom,
            incomingCallInvitation: null
          }
        } else {
          return {
            ...state,
            incomingCallInvitation: null
          }
        }
      }
      break;
    }

    case CALL_KICK: {
      const room = findRoom({ roomUid: action.roomUid, activeRooms: state.activeRooms})
      if (room) {
        // Im kicked
        if (action.toUserId === myId) {
          return {
            ...state,
            currentRoom: (room === state.currentRoom) ? null : state.currentRoom,
            activeRooms: state.activeRooms.filter( r => r.roomUid !== action.roomUid ),
            incomingCallInvitation: null
          }
        } else {
          // somebody in the room was kicked
          const updatedRoom = SetInRoom(room, {
            userState: updateUserCallState(room, [userInRoom(action.toUserId, room)], USERSTATE_KICKED )
          })
          return {
            ...state,
            ...activeRoomCanBeUpdatedInState(updatedRoom, state)
          }
        }
      }
      break;
    }

    default:
      return callsMedia(state, action, authState, messengerState)
  }

  return state
}
