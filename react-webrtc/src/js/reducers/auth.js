export const auth = (state = {}, action) => {
  switch (action.type) {
    case 'USER_REGISTERED':
      return  {
        ...state,
        me: {name: action.payload.name || '', id: action.payload.id},
        registered: true
      };
    case 'LOGIN': 
      return {
        ...state,
        loggedIn: action.id === state.me.id || state.loggedIn
      }
    case 'LOGOUT':
      return {
        ...state,
        loggedIn: action.id === state.me.id ? false : state.loggedIn
      }
    default:;
  }
  return state
}

