export const messenger = (state = {}, action, authState) => {
  switch (action.type) {
    case 'USERS_LOADED':
      return {
        ...state,
        onlineUsers: orderUsers(authState.me, action.onlineUsers || [])
      };
    case 'LOGIN': 
      if (!state.onlineUsers.find(u => action.id === u.id))
      {
        return {
          ...state,
          onlineUsers: orderUsers(authState.me, state.onlineUsers.concat({id: action.id, name: action.name}))
        };
      }
      break;
    case 'LOGOUT':
      return {
        ...state,
        onlineUsers: orderUsers(authState.me, state.onlineUsers.filter( u => u.id !== action.id))
      };
    default:;
  }
  return state
}


function orderUsers(me, onlineUsers) {
  const meOnline = onlineUsers.find( u => u.id === me.id );
  const meIndex = onlineUsers.indexOf(meOnline);
  if (meIndex > 0) {
    const arr = [...onlineUsers];
    arr.splice(0, 0, ...arr.splice(meIndex, 1));
    arr[0] = meOnline
    return arr;
  }
  return onlineUsers;
}
