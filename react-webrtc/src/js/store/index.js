// src/js/store/index.js
import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "../reducers";
import thunk from "redux-thunk";
import { webSocketMiddleware } from '../middleware/websocket'
import { loginMiddleware } from '../middleware/login'
import { translationMiddleware } from '../middleware/translation'

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [thunk];

if (process.env.NODE_ENV === "development") {
  const { createLogger } = require("redux-logger");
  middleware.push(createLogger());
}

middleware.push(webSocketMiddleware)
middleware.push(loginMiddleware)
middleware.push(translationMiddleware)

const store = createStore(
  rootReducer,
  storeEnhancers(applyMiddleware(...middleware))
);

export default store;