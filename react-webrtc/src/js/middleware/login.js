import {wsSend} from '../actions/websocket'

export function loginMiddleware(store) {
  return function(next) {
    return function(action) {
      switch (action.type) {
        case 'WS_CONNECTED':
          store.dispatch(wsSend({
            type: 'LOGIN',
            id: store.getState().auth.me.id,
            name: store.getState().auth.me.name
          }))
          break;
        default:;
      }
      next(action)
    }
  }
}