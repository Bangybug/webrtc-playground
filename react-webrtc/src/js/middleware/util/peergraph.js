import { CALLTYPE_INITIATOR } from "../../actions/translation"

//
// Implementation of peer connections establishment strategy
// The goal: everyone is connected to each other in a room
//

/*
Controls how users connect to each other in a 1-ranked peer-2-peer conference.
This is only related to connections, has nothing to do with streaming state of each peer. 
When everyone starts streaming, the bandwidth will be quickly occupied and the connection will degrade.
The goal: test available bandwidth.

Typical case:

1) Invitation to call:
- User A invites B,C,D.  
- B,C,D respond to invitation to A. 
- User A initiates connections: A-B, A-C, A-D.
- User B initiates connections: B-C, B-D.
- User C initiates connections: C-D
4 users, 6 connections

2) Invitation of E by D:
- User E responds to invitation to D.
- User D initiates D-E.
- User A initiates A-E.
- User B initiates B-E.
- User C initiates C-E.
4+1 users, 6+4 connections.

To avoid extra negotiation/arbitration on which users should initiate connections, lets benefit on the fixed invitation order. That means:
- The user who has invited others begins initiating connections to the invited users.
- Invited users individually accept invitations and report to the room at their own order and pace.
- All users in the room who have not yet connected to the invited users, also initiate connections to the invited users.
- A group of invited users must interconnect between themselves. This may look trick-ish:
- Invited user enters the room with joinCall. He must accept incoming connections from users already within a call.
- Next invited user enters the room, he gets the same invitation with the same set of users. He'll get incoming connections from users he knew from his invitation.
But also he'll get extra connection from the previous invited user. That's how it can work.
*/
export const P2PGraph = (store) => {
  return {

    onInvitedUserCallAccept: function({room, userId, invitedUserId, invitedByUserId, handleSelfCall}) {
      const ret = [];
      if (userId !== invitedUserId || handleSelfCall) {
        ret.push({
          fromUserId: userId,
          toUserId: invitedUserId,
          callType: CALLTYPE_INITIATOR,
          roomUid: room.roomUid,
          media: room.media
        })
      }
      return ret
    } 

  }
}