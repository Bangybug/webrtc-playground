import {rtcServersConfig} from '../../../config'
import {fixConstraints, startScreenCapture} from '../../util/rtc'
import {CALLTYPE_INITIATOR, log_, sendIceCandidate_, sendOffer_, streamUpdate_, sendAnswer_, STREAMTYPE_REMOTE, STREAMTYPE_DISPLAY, STREAMTYPE_USERMEDIA, TRACKKIND_SCREEN, TRACKKIND_AUDIO, TRACKKIND_VIDEO} from '../../actions/translation'
import { findLocalStream, findLocalStreamAndTrack } from '../../reducers/util/room'

//
// Routines for managing RTCPeerConnections in conference rooms
//

export const PeerConnections = (store) => {
  const peerConnections = {}

  function reportError(e) {
    // TODO implement error signaling
    console.error(e);
  }

  function closeConnection(conn) {
    if (conn.pc) {
      const pc = conn.pc
      pc.ontrack = pc.onremovetrack = pc.onremovestream = 
      pc.onicecandidate = pc.oniceconnectionstatechange = 
      pc.onsignalingstatechange = pc.onicegatheringstatechange = pc.onnegotiationneeded = null;
      try {
        pc.close();
      } 
      catch (e) {
        reportError(e)
      }
    }
  }

  function stopStreamTracks(localStream) {
    const streamTracks = localStream.getTracks()
    for (const track in streamTracks) { 
      try {
        if (!track.ended) {
          track.stop()
        }
      } catch (e) {
        reportError(e)
      }
    }
  }

  function stopLocalTracks(userStreams) {
    const userStream = findLocalStream(userStreams)
    if (userStream) {
      for (const streamId in userStream.userMediaStreams) {
        stopStreamTracks( userStream.userMediaStreams[streamId] )
      }
      for (const streamId in userStream.displayMediaStreams) {
        stopStreamTracks( userStream.displayMediaStreams[streamId] )
      }
    }
  }

  function onUnmuteLocalTrack({roomUid, callType, streamType}/*, e*/) {
    store.dispatch(streamUpdate_({ 
      roomUid,
      callType,
      streamType
    }))
  }

  function onRemoteStreamActivityChange({roomUid, callType, fromUserId}, e) {
    if (!checkIfStreamEnded(e.target, roomUid)) {
      store.dispatch(streamUpdate_({ 
        fromUserId: fromUserId, 
        roomUid,
        callType,
        stream: e.target,
        streamType: STREAMTYPE_REMOTE
      }))
    }
  }

  function addExistingLocalTracksToPeerConnection(pc, fromUserId, roomUid, callType, userStreams) {
    console.log('addExistingLocalTracksToPeerConnection', userStreams)
    const userStream = findLocalStream(userStreams)
    let addedTracks = false
    if (userStream) {
      const streamTypes = [STREAMTYPE_DISPLAY, STREAMTYPE_USERMEDIA]
      const media = [userStream.displayMediaStreams, userStream.userMediaStreams]
      for (let i=0; i<media.length; ++i) {
        for (const streamId in media[i]) {
          
          const localStream = media[i][streamId]
          const streamTracks = localStream.getTracks()

          for (const track of streamTracks) {
            const trackSender = pc.getSenders().find(function (sender) { return sender.track === track; })
            if (!trackSender && track.enabled) {
              console.log('\tadded existing local track', track.id, track.label, "enabled?=",track.enabled, "tracksender?=",!!trackSender, "ended?=",track.ended)
              addedTracks = true
              pc.addTrack(track, localStream)
            }
          }

          if (addedTracks) {
            store.dispatch(streamUpdate_({ 
              fromUserId: fromUserId, 
              roomUid,
              callType,
              stream: localStream,
              streamType: streamTypes[i]
            }))
          }
        }
      }
    }

    return addedTracks
  }

  function findPeerConnectionInternal({roomUid, callType, fromUserId, toUserId}) {
    const roomConn = peerConnections[roomUid] = peerConnections[roomUid] || {}
    const isSelfCall = fromUserId === toUserId
    let connDef
    if (isSelfCall) {
      // callType is only need to distinguish sender and receiver in degenerative self-calling case
      connDef = roomConn[callType] && roomConn[callType].find( c => (c.fromUserId === fromUserId && c.toUserId === toUserId) )
    } else {
      // in normal case, one browser holds only one connection (it's still stored by callType)
      connDef = Array.prototype.concat( ...Object.keys(roomConn).map( callType => roomConn[callType] ) )
        .find( c => (c.fromUserId === fromUserId && c.toUserId === toUserId) )
    }
    return connDef
  }

  function updateLocalTrackInPeerConnections(myConnections, track, trackStream, streamingEnabled) {
    for (const connDef of myConnections) {
      if (!connDef.pc || !connDef.canStreamOwnMedia) { continue }
      const pc = connDef.pc
      try {
        const trackSender = pc.getSenders().find(function (sender) { return sender.track === track; })
        track.enabled = streamingEnabled
        if (streamingEnabled) {
          if (!trackSender) {
            pc.addTrack(track, trackStream)
          }
        } else {
          if (trackSender) {
            pc.removeTrack( trackSender )
            // removing and/or enabling=false does not stop local tracks, camera led will be still on - so we stop the track,
            // and it cannot be restarted next time, so we've got to requery it then
            track.stop() 
          }
        }
      }
      catch (e) {
        reportError(e)
      }
    }
  }

  function checkIfStreamEnded(stream, roomUid) {
    let ended = true
    if (typeof stream.active !== 'undefined') {
      ended = !stream.active
    } else {
      ended = !stream.getTracks().some( t => {
        return (t.readyState !== 'ended' && t.enabled)
      })
    }
    if (ended) {
      store.dispatch(streamUpdate_({ 
        roomUid,
        stream,
        isRemoved: true
      }))
    }
    return ended
  }

  function acceptAnswer(connDef, callerAnswer, userStreams) {
    const {pc} = connDef
    pc.setRemoteDescription(new RTCSessionDescription(callerAnswer))
      .catch( reportError )
  }

  function acceptOffer(connDef, callerOffer, userStreams) {
    const {pc, fromUserId, toUserId, callType, roomUid} = connDef
    pc.setRemoteDescription(new RTCSessionDescription(callerOffer))
      .then( () => addExistingLocalTracksToPeerConnection(pc, fromUserId, roomUid, callType, userStreams) )
      .then(() => {
        return pc.createAnswer()
      })
      .then( answer => {
        if (answer) {
          return pc.setLocalDescription(answer);
        }
      })
      .then( () => {
        store.dispatch( sendAnswer_( {fromUserId, toUserId, answer: pc.localDescription.toJSON(), roomUid} ))
      })
      .catch( reportError )
  }

  function sendOffer(pc, fromUserId, toUserId, roomUid, media) {
    pc.createOffer()
      .then( (offer) => pc.setLocalDescription(offer))
      .then( () => {
        store.dispatch(sendOffer_({ 
          fromUserId, 
          toUserId, 
          offer: pc.localDescription.toJSON(),
          roomUid,
          media
        }))
      })
      .catch(reportError)
  }

  return {
    findPeerConnection: function ({roomUid, callType, fromUserId, toUserId}) {
      const connDef = findPeerConnectionInternal({roomUid, callType, fromUserId, toUserId})
      return connDef && connDef.pc
    },

    updateLocalTrackStreamingState: function(roomUid, trackKind, userStreams, streamingEnabled) {
      const roomConn = peerConnections[roomUid] || {}
      const myConnections = Array.prototype.concat( ...Object.keys(roomConn).map( callType => roomConn[callType] ) )
      const userStream = findLocalStream(userStreams)
      let {trackStream, track} = findLocalStreamAndTrack(trackKind, userStream) || {}

      console.log('PEERCONNECTIONS',peerConnections, myConnections, 'hasstream',!!track)

      // when enabling tracks, we should query them via getUserMedia or getDisplayMedia, depending on the trackKind
      // when disabling tracks, we just remove them from stream, stop them and forget
      // by design, every room is sending own streams with own tracks
      if (track) {
        updateLocalTrackInPeerConnections(myConnections, track, trackStream, streamingEnabled)
        checkIfStreamEnded(trackStream, roomUid)
      } else if (streamingEnabled) {
        let promise
        if (trackKind === TRACKKIND_SCREEN) {
          promise = startScreenCapture()
        } else {
          const constraints = { 
            video: TRACKKIND_VIDEO === trackKind,
            audio: TRACKKIND_AUDIO === trackKind
          }
          promise = fixConstraints(constraints)
            .then( constraints => { 
              return navigator.mediaDevices.getUserMedia(constraints) 
            })
        }
        
        promise.then( (stream) => {
          if (stream) {
            console.log('adding tracks ', stream.getTracks())
            const streamType = trackKind === TRACKKIND_SCREEN ? STREAMTYPE_DISPLAY : STREAMTYPE_USERMEDIA

            stream.getTracks().forEach( t => {
              updateLocalTrackInPeerConnections(myConnections, t, stream, true) 
              t.onunmute = t.onunmute || onUnmuteLocalTrack.bind(this, {roomUid, callType:CALLTYPE_INITIATOR, streamType});
            })

            store.dispatch(streamUpdate_({ 
              roomUid,
              callType: CALLTYPE_INITIATOR,
              stream,
              streamType
            }))
          }
        })
        .catch( reportError );
        
      }
    },


    setPeerConnection: function({fromUserId, toUserId, callType, roomUid, media, callerAnswer, callerOffer}, {userStreams}) {
      // in degenerative cases of self-calling, one party should not stream,  only listen to avoid feedback
      const isSelfCall = fromUserId === toUserId;
      const canStreamOwnMedia = !isSelfCall || callType === CALLTYPE_INITIATOR;

      const connDef = findPeerConnectionInternal({roomUid, callType, fromUserId, toUserId}) || 
        {fromUserId, toUserId, roomUid, canStreamOwnMedia}

      let pc = connDef.pc
      if (!connDef.pc) {
        pc = connDef.pc = new RTCPeerConnection(rtcServersConfig)

        pc.oniceconnectionstatechange = e => {
          // TODO find out how this can be used
          store.dispatch(log_({statechanged: pc.iceConnectionState}))
        }
      
        pc.onicecandidate = ({candidate}) => {
          if (candidate) {
            store.dispatch(sendIceCandidate_({fromUserId, toUserId, candidate: candidate.toJSON(), roomUid, callType}))
          }
        }
      
        pc.onnegotiationneeded = () => {
          //console.log('NEGOTIATION NEEDED')
          sendOffer(pc, fromUserId, toUserId, roomUid, media)
        }
      
        // Once remote track media arrives, let UI code know about it.
        // the semantics from-to is reversed, because this event notifies about remote stream changes, and remote stream is coming from toUserId for which
        // this peer connection (pc) is created
        pc.ontrack = (e) => {
          const stream = e.streams && e.streams[0]
          if (stream) {
            if (!stream.onactive) {
              stream.onactive = stream.oninactive = onRemoteStreamActivityChange.bind(this,  {roomUid, callType, fromUserId});
            }
            store.dispatch(streamUpdate_({ 
              fromUserId: toUserId, 
              roomUid,
              callType,
              stream,
              streamType: STREAMTYPE_REMOTE
            }))
          }
        }
      
        pc.onremovetrack = (e) => {
          // when the last track is removed, this event is normally not sent, remote stream becomes inactive instead
          store.dispatch(streamUpdate_({ 
            fromUserId: toUserId, 
            roomUid,
            callType,
            streamType: STREAMTYPE_REMOTE
          }))
        }
      }

      const roomConn = peerConnections[roomUid] = peerConnections[roomUid] || {}
      const callConn = roomConn[callType] = roomConn[callType] || [];
      if (-1 === callConn.indexOf(connDef)) {
        callConn.push(connDef)
      }

      if (callerAnswer) {
        acceptAnswer(connDef, callerAnswer, userStreams)
      } else if (callerOffer) {
        acceptOffer(connDef, callerOffer, userStreams)
      } else {
        // if initiating a new connection to new peer, lets add current active tracks from local streams to the new connection
        const hasTracks = addExistingLocalTracksToPeerConnection(pc, fromUserId, roomUid, callType, userStreams)
        if (!hasTracks) {
          // if user is completely muted, he wont generate an offer via onnegotiationneeded, 
          // but we can send an empty offer to establish a connection
          sendOffer(pc, fromUserId, toUserId, roomUid, media) 
        }
      }
    },

    closeRoomConnectionsToPeer: function({roomUid, userStreams}, peerId)  {
      const roomConn = peerConnections[roomUid]
      if (roomConn) {
        const myConnections = Array.prototype.concat( ...Object.keys(roomConn).map( callType => roomConn[callType] ) )
        for (const conn of myConnections) {
          if (conn.toUserId === peerId || conn.fromUserId === peerId) {
            closeConnection(conn)
          }
        }
      }
    },

    closeRoomConnections: function({roomUid, userStreams}) { 
      // shutdown all peer connections
      const roomConn = peerConnections[roomUid]
      if (roomConn) {
        const myConnections = Array.prototype.concat( ...Object.keys(roomConn).map( callType => roomConn[callType] ) )
        for (const conn of myConnections) {
          closeConnection(conn);
        }
        delete peerConnections[roomUid]
        // clean dom controls? - the component will be unmounted in the UI outer code
    
        // TODO notify peers? - it is done in oniceconnectionstatechange by "disconnected" event
        // But the said state change is rather a transport level event, based on just that we cannot say whether the remote user did just hang up or had
        // temporary connection problems, and that event can be a result of connection renegotiation, so there will be another connection soon.
        // Therefore it's not good to use it for peers notification.
      }
      stopLocalTracks(userStreams)
    }
  }

 
  
}