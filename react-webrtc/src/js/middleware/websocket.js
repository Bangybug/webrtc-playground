import { wsConnected, wsDisconnected, startWebsocket, wsSend } from '../actions/websocket'

// * websocket handling goes here,  actions such as login, call, hangup, etc are handled via "WS_SEND", the go-messenger service merely 
// retranslates what is sent for all users (including sender), thus the said actions are getting dispatched from "onMessage", 
// * websocket is just a transport to deliver dispatch calls to all connected clients which sounds reasonable and is not prohibiting other transport

export function webSocketMiddleware(store) {
  let socket = null;
  const sendOnConnect = [];
  let runningTimerId;
  let connected = false;

  const onOpen = store => (event) => {
    store.dispatch(wsConnected(event.target.url));
  };

  const onClose = store => (event) => {
    store.dispatch(wsDisconnected('', event.code));
  };

  const sendNext = () => {
    if (sendOnConnect.length) {
      store.dispatch(wsSend(sendOnConnect.pop()))
    }
  }

  const onMessage = store => (event) => {
    let bracesOpen = 0;
    let msg = '';
    for (let i=0; i<event.data.length; ++i) {
      const c = event.data.charAt(i);

      if (bracesOpen > 0) {
        msg += c
      }

      if ('{' === c) {
        if (!msg.length) {
          msg += c
        }
        ++bracesOpen
      } else if ('}' === c) {
        if (--bracesOpen === 0) {
          let json
          try {
            msg = msg.split('\\"').join('"')
            json = JSON.parse(msg)
          } catch (e) {
            console.error('Cannot parse ws message', msg)
            console.error(e)
          }
          msg = ''

          try {
            if (json.type) {
              store.dispatch(json)
            } else {
              console.log(json);
            }
          } catch (e) {
            console.error('Error dispatching ws message', json)
            console.error(e)
          }
        }
      }
    }
  };


  return function(next) {
    return function(action) {
      switch (action.type) {
        case 'WS_CONNECT':
          if (socket !== null) {
            socket.close();
          }
          // connect to the remote host
          socket = new WebSocket(action.host);

          // websocket handlers
          socket.onmessage = onMessage(store);
          socket.onclose = onClose(store);
          socket.onopen = onOpen(store);
          break;

        case 'WS_DISCONNECT':
          if (socket !== null) {
            socket.close();
          }
          socket = null;
          break;

        case 'WS_CONNECTED': {
          connected = true;
          sendNext();
          return next(action);
        }

        case 'WS_DISCONNECTED': {
          connected = false;
          console.log('disconnected', runningTimerId)
          if (!runningTimerId) {
            runningTimerId = setTimeout(() => {
              runningTimerId = null
              console.log('restarting websocket')
              store.dispatch( startWebsocket(store.getState().auth.me.id) )
            }, 1000);
          }
          break;
        }
        
        case 'WS_SEND':
          try {
            if (!connected) {
              sendOnConnect.push(JSON.stringify(action.json))
            } else {
              socket.send(JSON.stringify(action.json))
              sendNext(); // recursion
            }
          }
          catch (e) {
            sendOnConnect.push(JSON.stringify(action.json))
            store.dispatch( wsDisconnected('', 'onsend') )
          }
          break;

        default:
          return next(action);
      }
    };
  };
}