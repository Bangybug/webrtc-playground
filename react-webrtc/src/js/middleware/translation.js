import { callInvitePending_, CALLTYPE_INITIATOR, CALLTYPE_ACCEPTOR, CALL_INVITE, CALL_INVITE_ACCEPTED, callInvite_, CALL_MAKE, CALL_HANGUP, CALL_KICK, availableTracksUpdate_ } from '../actions/translation'
import { PeerConnections } from './util/peerconnections'
import { P2PGraph } from './util/peergraph'
import {findRoom} from '../reducers/util/room'
import { fixConstraints } from '../util/rtc';

// FYI a note to myself about middlewares in redux. This middleware spawns actions, but the spawned actions will be executed and reduced in a first place,
// before original action is reduced! Referencing state object may give ambiguous results if things were not thought well!!! 


export function translationMiddleware(store) {

  const connMgr = PeerConnections(store);
  const graph = P2PGraph(store);
  let handleSelfCall = false;

  function reportError(e) {
    // TODO implement error signaling
    console.error(e);
  }

  return function(next) {
    return function(action) {

      const myId = store.getState().auth.me.id
      const myName = store.getState().auth.me.name
      const onlineUsers = store.getState().messenger.onlineUsers
      const activeRooms = store.getState().calls.activeRooms

      switch (action.type) {

        case 'LOGIN': {
          fixConstraints({audio: true, video: true})
            .then( constraints => store.dispatch(availableTracksUpdate_({constraints})) )
            .catch( reportError )
          break;
        }

        // local action
        // started by current user, broadcasted as CALL_INVITE to multiple parties
        case CALL_MAKE: {
          store.dispatch(callInvite_({
            media: action.room.media,
            callerId: myId,
            callerName: myName,
            roomUid: action.room.roomUid,
            callerRoomName: action.room.name,
            calleeIds: action.room.users.map(u => u.id),
            fromUserId: myId
          }))
          handleSelfCall = action.room.users.some(u => u.id === myId)
          break;
        }

        // broadcasted via websocket to multiple parties at once
        case CALL_INVITE: {
          // incoming call? 
          // lets store invitation in the state, the UI should pick it up
          // TODO check if already joined 
          if (action.calleeIds.indexOf(myId) !== -1) {
            // or join immediately
            //joinCall(store.getState().me, action);
            store.dispatch(callInvitePending_({
              ...action, 
              invitedBy: onlineUsers.find( u => u.id === action.fromUserId )
            }))
          }
          break;
        }

        // broadcasted via websocket, sent by individual user to indicate ready for offers state
        case CALL_INVITE_ACCEPTED: {
          // if this user is in the room where invitation takes place
          const room = findRoom({roomUid: action.roomUid, activeRooms})
          if (room) {
            const toCall = graph.onInvitedUserCallAccept({room, userId:myId, invitedUserId: action.fromUserId, invitedByUserId: action.toUserId, handleSelfCall})
            for (const conn of toCall) {
              if (connMgr.findPeerConnection(conn)) {
                reportError("Calling the same peer multiple times.") // should not happen
              } else {
                connMgr.setPeerConnection(conn, room)
              }
            }
          }

          if (myId === action.fromUserId && myId === action.toUserId) {
            handleSelfCall = false;
          }
          break;
        }

        // broadcasted via websocket
        case 'RTC_OFFER': {
          if (action.toUserId === myId) {
            // TODO if the room was closed by caller at the time of remote acceptance, the callee doesnt know about it and wil have to wait
            const room = findRoom({roomUid: action.roomUid, activeRooms})
            if (room) {
              connMgr.setPeerConnection({
                fromUserId: myId,
                toUserId: action.fromUserId,
                callType: CALLTYPE_ACCEPTOR,
                roomUid: action.roomUid,
                media: action.media,
                callerOffer: action.offer
              }, room)
            }
          }
          break;
        }

        // broadcasted via websocket
        case 'RTC_ANSWER': {
          if (action.toUserId === myId) {
            const room = findRoom({roomUid: action.roomUid, activeRooms})
            if (room) {
              connMgr.setPeerConnection({
                fromUserId: myId,
                toUserId: action.fromUserId,
                callType: CALLTYPE_INITIATOR,
                roomUid: action.roomUid,
                media: room.media,
                callerAnswer: action.answer
              }, room)
            }
          }
          break;
        }

        // broadcasted via websocket
        case 'ICE_CANDIDATE': {
          // exchange network addresses between peers
          if (action.toUserId === myId) {
            const pc = connMgr.findPeerConnection({
              roomUid: action.roomUid,
              callType: action.callType === CALLTYPE_INITIATOR ? CALLTYPE_ACCEPTOR : CALLTYPE_INITIATOR,
              fromUserId: myId,
              toUserId: action.fromUserId
            })
            console.log('PC found for candidate', !!pc)
            if (pc) {
              pc.addIceCandidate(new RTCIceCandidate(action.candidate))
                .catch( reportError );
            }
          }
          break;
        }

        // broadcasted via ws
        case CALL_HANGUP: {
          // did I just hang up?
          if (action.fromUserId === myId) {
            connMgr.closeRoomConnections({roomUid: action.roomUid})
          }
          break;
        }


        // broadcasted via ws
        case CALL_KICK: {
          const room = findRoom({roomUid: action.roomUid, activeRooms})
          if (room) {
            // I'm kicked - close all peer connections
            if (action.toUserId === myId) {
              connMgr.closeRoomConnections(room)
            } else {
              // Somebody in the room was kicked
              // can close peer connection proactively and/or can leave that job to the peer being kicked
              connMgr.closeRoomConnectionsToPeer(room, action.toUserId)
            }
          }
          break;
        }


        case 'LOCALTRACK_STREAM_UPDATE': {
          connMgr.updateLocalTrackStreamingState(action.roomUid, action.trackKind, action.userStreams, action.streamingEnabled)
          break;
        }

        default:;
      }
      next(action)
    }
  }
}
