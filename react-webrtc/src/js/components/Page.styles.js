export default {
  root: {
    overflow: 'hidden',
    height: '100%',
    position: 'relative',
    flexDirection: 'column',
    backgroundColor: '#ffffff',
    //minHeight: '100vh'
  },
  header: {
    position: 'fixed',
    width: '100%',
    zIndex: '1200'
  },
  navigation: {
    width: '240px',
    float: 'left',
    height: '100%'
  },
  navigationBottom: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderTop: '1px solid #ccc'
  },
  navigationOverflow: {
    width: '240px',
    height: '100%',
    backgroundColor: '#fff',
    zIndex: '100',
    position: 'relative',
    borderRight: '1px solid #ccc',
    overflow: 'hidden',
    transition: 'width 0.2s'
  },
  navigationBody: {
    width: '240px',
    height: '100%',
    overflow: 'auto'
  },
  content: {
    overflow: 'hidden',
    height: '100%',
    flexGrow: '2',
    position: 'relative',
    WebkitOverflowScrolling: 'touch',
    zIndex: '1',
    //minHeight: '100vh'
  },
  topBar: {
    borderBottom: '1px solid #ccc',
    backgroundColor: '#ffffff',
    width: '100%'
  },
  body: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
    backgroundColor: '#fafafa'
  },
  rightSidePanel: {
    float: 'right',
    marginRight: '-490px',
    height: '100%',
    borderLeft: '1px solid #ccc',
    width: '490px',
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
    position: 'relative',
    zIndex: '100',
    backgroundColor: '#fff',
    transition: 'margin-right 0.2s',
    boxShadow: '0px 1px 8px 0px rgba(0, 0, 0, 0.2),0px 3px 4px 0px rgba(0, 0, 0, 0.14),0px 3px 3px -2px rgba(0, 0, 0, 0.12)',
    '&[open]': {
      marginRight: 0
    }
  },
  errorPopupWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '64px',
    width: '100%',
    pointerEvents: 'none'
  },
  errorPopup: {
    position: 'relative',
    minWidth: '600px',
    maxWidth: '1000px',
    margin: '0 auto',
    top: 0,
    left: 'auto',
    right: 'auto',
    pointerEvents: 'all'
  },
  progress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  }
};
