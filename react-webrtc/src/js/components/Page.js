import React, { PureComponent }  from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import { withStyles } from '@material-ui/styles';
import styles from './Page.styles';


export class Page extends PureComponent {
  static defaultProps = {
    classes: {},
    Header: false,
    TopBar: false,
    Navigation: false,
    NavigationBottom: false,
    RightSidePanel: false,
    openRightSidePanel: false,
    fullWidth: false,
    disablePadding: false
  };

  render () {
    const {
      Header,
      TopBar,
      Navigation,
      NavigationBottom,
      RightSidePanel,
      children,
      openRightSidePanel,
      fullWidth,
      classes,
      disablePadding,
      isLoading
    } = this.props;

    return (
      <div className={classes.root}>
        {Header && (
          <div className={classes.header}>
            <Header />
          </div>
        )}
        <div style={{ height: '100%', paddingTop: '48px' }}>
          {Navigation && (
            <div className={classes.navigation}>
              <div className={classes.navigationOverflow}>
                <div className={classes.navigationBody}>
                  <Navigation />
                </div>
              </div>
            </div>
          )}
          <div className={classes.body}>
            {TopBar && (
              <Toolbar className={classes.topBar}>
                <TopBar />
              </Toolbar>
            )}
            <div className={classes.content}>
              {isLoading && (
                <div className={classes.progress}>
                
                </div>
              )}
              {RightSidePanel && (
                <div className={classes.rightSidePanel} open={openRightSidePanel}>
                  <RightSidePanel />
                </div>
              )}
              <div id='contentBody' style={{ overflow: 'auto', height: '100%' }}>
                <div style={{
                  minWidth: '600px',
                  maxWidth: fullWidth ? '100%' : '1045px',
                  padding: disablePadding ? '0' : '24px',
                  height: '100%',
                  margin: '0 auto'
                }}>
                  {children}
                </div>
              </div>
            </div>
            {NavigationBottom && (
              <div
                style={{
                  minWidth: '600px',
                  maxWidth: '100%',
                  minHeight: 56,
                  margin: '0 auto'
                }}
                className={classes.navigationBottom}>
                <NavigationBottom />
              </div>
            )}
            
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Page);
