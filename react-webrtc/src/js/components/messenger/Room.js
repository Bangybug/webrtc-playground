import React, { PureComponent }  from 'react';
import { connect } from "react-redux";
import { Button, Typography, Chip, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import Styles from './Room.styles'
import {makeCall, hangupCall, inviteToCall} from '../../actions/translation'
import UserMediaStream from '../translation/UserMediaStream'
import { USERSTATE_CONNECTED, USERSTATE_INVITED, USERSTATE_WAITING, findLocalStream, findRemoteStream, getUserState } from '../../reducers/util/room';
import OutgoingStreamControl from '../translation/OutgoingStreamControl';

// not sure how good is the idea of using redux for distributing ui events across the app, but lets try
const mapStateToProps = state => {
  return { 
    userClicked: state.ui.userClicked,
    me: state.auth.me
  };
};

export class Room extends PureComponent {
  static defaultProps = {
    currentRoom: {}
  }

  state = {
    users: [],
    inviteUsers: [],
    roomName: ''
  }

  componentDidUpdate(prevProps, prevState) {
    // hm... responding to actions in UI via redux is definitely not very clear
    // maybe because I still resist of moving all state logic to redux layer
    // redux is for business logic, but I still think some components may have some UI logic, e.g. handleUserToggle that needs not be in redux
    if (prevProps.currentRoom !== this.props.currentRoom) {
      this.setState({
        roomName: this.props.currentRoom.name || '', 
        users: this.props.currentRoom.users || [],
        inviteUsers: []
      })
    }
    if (!this.props.currentRoom.callInProgress && prevProps.userClicked !== this.props.userClicked) {
      this.handleUserToggle(this.props.userClicked)
    }
    if (this.props.currentRoom.callInProgress && prevProps.userClicked !== this.props.userClicked) {
      this.handleInviteUserToggle(this.props.userClicked)
    }
  }

  handleUserToggle = (userClicked) => {
    const hasThatUser = this.state.users.some( u => u.id === userClicked.id);
    if (hasThatUser) {
      this.setState({users: this.state.users.filter( u => u.id !== userClicked.id) })
    } else {
      this.setState({users: this.state.users.concat(userClicked) })
    }
  }

  handleInviteUserToggle = (userClicked) => {
    const { currentRoom, me } = this.props
    const roomUserState = currentRoom.userState || {}
    const userState = roomUserState[userClicked.id]
    const canInvite = me.id !== userClicked.id && (
      !userState || ([USERSTATE_CONNECTED, USERSTATE_INVITED, USERSTATE_WAITING].indexOf(userState) === -1))
      
    if (canInvite) {
      const hasThatUser = this.state.inviteUsers.some( u => u.id === userClicked.id);
      if (hasThatUser) {
        this.setState({inviteUsers: this.state.inviteUsers.filter( u => u.id !== userClicked.id) })
      } else {
        this.setState({inviteUsers: this.state.inviteUsers.concat(userClicked) })
      }
    }
  }

  handleDelete = (e) => {
    this.handleUserToggle(this.state.users[Number(e.currentTarget.id)])
  }

  handleDeleteInvitedUser = (e) => {
    this.handleInviteUserToggle(this.state.inviteUsers[Number(e.currentTarget.id)])
  }


  handleNameChange = (e) => {
    this.setState({roomName: e.target.value})
  }

  validate = () => {
    const {currentRoom} = this.props;
    const {users, roomName} = this.state;
    return users.length && roomName && !currentRoom.callInProgress;
  }

  handleCallStart = () => {
    const {currentRoom, makeCall} = this.props;
    const {users, roomName} = this.state;
    const roomConfig = {
      ...currentRoom,
      users,
      name: roomName
    }
    makeCall(roomConfig)
  }

  handleHangup = () => {
    const {currentRoom, me, hangupCall} = this.props;
    hangupCall({ fromUserId: me.id, roomUid: currentRoom.roomUid })
  }

  handleInviteUsers = () => {
    const {inviteUsers} = this.state;
    const {currentRoom, me} = this.props;
    this.props.inviteToCall(me, currentRoom, inviteUsers.map(u => u.id))
  }

  render () {
    const {classes, currentRoom, me} = this.props;
    const {users, roomName, inviteUsers} = this.state;
    const p2p = Boolean(currentRoom.p2p);
    const {handleDelete, validate, handleNameChange, handleCallStart, handleHangup, handleInviteUsers, handleDeleteInvitedUser} = this;

    return (
      <>
        {!currentRoom.callInProgress && ( 
          <>
            <div className={classes.paper}>
              <Typography variant="subtitle1">
                {p2p ? 'Select users for a peer-to-peer call and enter room name' : 'Please select users for a group call and enter room name'}
              </Typography>

              <TextField label="Room name" value={roomName || ''} onChange={handleNameChange}/>
            </div>

            <div className={classes.paper}>
              {users.map( (u,i) => (
                <Chip
                  className={classes.chip}
                  id={i}
                  key={u.id}
                  icon={<img className={classes.avatar} alt={u.name} src={`${process.env.PUBLIC_URL}/icons/animals/${u.name}.svg`}/>}
                  label={u.name}
                  onDelete={handleDelete}
                  disabled={currentRoom.callInProgress}
                />
              ))}
            </div>
          </>
        )}

        <Button variant="contained" color="primary" disabled={!validate()} onClick={handleCallStart}>
          {currentRoom.callInProgress ? 'Call in progress...' : 'Call'}
        </Button>
        
        &nbsp;

        {currentRoom.callInProgress && (
          <>
            <Button variant="contained" color="primary" onClick={handleHangup}>
              Hang up
            </Button>  

            <div className={classes.indented}>
              <Typography variant="subtitle1">
                Your streaming options
              </Typography>

              <OutgoingStreamControl user={me} userState={getUserState(currentRoom, me)} stream={findLocalStream(currentRoom.userStreams)} room={currentRoom} />
            </div>

            <div className={classes.indented}>
              <Typography variant="subtitle1">
                Users in this call
              </Typography>
            </div>
          </>
        )}

        {currentRoom.callInProgress && currentRoom.users.map( u => (
          <UserMediaStream key={u.id} user={u} userState={getUserState(currentRoom, u)} stream={findRemoteStream(currentRoom, u)} roomUid={currentRoom.roomUid} />
        ))}

        {currentRoom.callInProgress && (
          <div className={classes.newLine}>
            <div className={classes.paper}>
              <Typography variant="subtitle1">
                Invite more users? (cannot invite users in connected state)
              </Typography>
            </div>

            <div className={classes.paper}>
              {inviteUsers.map( (u,i) => (
                <Chip
                  className={classes.chip}
                  id={i}
                  key={u.id}
                  icon={<img className={classes.avatar} alt={u.name} src={`${process.env.PUBLIC_URL}/icons/animals/${u.name}.svg`}/>}
                  label={u.name}
                  onDelete={handleDeleteInvitedUser}
                />
              ))}
            </div>

            <Button variant="contained" color="primary" disabled={!inviteUsers.length} onClick={handleInviteUsers}>
              Invite more
            </Button>  
          </div>
        )}

      </>
    );
  }
}

const CRoom = connect(mapStateToProps, {makeCall, hangupCall, inviteToCall})( withStyles(Styles)(Room) );
export default CRoom;
//export default withStyles(Styles)(Room);