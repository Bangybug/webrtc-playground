export default theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper
  },

  topBar: {
    borderBottom: '1px solid #ccc',
    backgroundColor: '#ffffff',
    width: '100%'
  },

  title: {
    flexGrow: 1
  }

})
