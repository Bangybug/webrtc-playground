import React, { PureComponent }  from 'react';
import { Menu, MenuItem, IconButton } from '@material-ui/core'
import { Add } from '@material-ui/icons'
import { connect } from "react-redux";
import { createRoom, describeCallMedia } from '../../actions/translation';

const options = [
  {id:'p2p-audio', label:'Peer to peer audio', p2p: true, media: describeCallMedia({
    allowAudio: true})},
    
  {id:'p2p-unrestricted', label:'Peer to peer audio/video/screen', p2p: true, media: describeCallMedia({
    allowAudio: true, 
    allowVideo: true,
    allowScreenCapture: true
  })}
]

export class CreateRoomMenu extends PureComponent {
 
  state = {
    anchorEl: null
  }

  handleClick = event => {
    this.setState({anchorEl: event.currentTarget});
  };

  handleClose = () => {
    this.setState({anchorEl: null});
  };

  handleItemClick = (e) => {
    const opt = options[Number(e.currentTarget.id)]
    this.props.createRoom({p2p: opt.p2p, media: opt.media })
    this.handleClose()
  }

  render () {
    const {handleClick, handleClose, handleItemClick} = this;
    const {anchorEl} = this.state;
    return (
    <div>
      <IconButton
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <Add />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {options.map( (option,i) => (
          <MenuItem key={option.id} id={i} onClick={handleItemClick}>
            {option.label}
          </MenuItem>
        ))}
      </Menu>
    </div>

    );
  }
}

export default connect(null, {createRoom})( CreateRoomMenu );

