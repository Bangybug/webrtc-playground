import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import blue from '@material-ui/core/colors/blue';

import { Button, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';

const styles = {
  cancelButton: {
    color: blue['500'],
    marginRight: '10px'
  }
};

export class ConfirmDialog extends PureComponent {
  static propTypes = {
    cancelBtnText: PropTypes.string,
    classes: PropTypes.object,
    confirmBtnText: PropTypes.string,
    content: PropTypes.string,
    okFirst: PropTypes.bool,
    onCancel: PropTypes.func,
    onConfirm: PropTypes.func,
    open: PropTypes.bool,
    title: PropTypes.string
  };

  static defaultProps = {
    classes: {},
    open: false
  };

  render () {
    const { open, title, content, onConfirm, onCancel, children, confirmBtnText, cancelBtnText, classes, okFirst } = this.props;

    const buttons = [
      <Button
        className={okFirst ? classes.cancelButton : ''}
        onClick={onConfirm}>
        {confirmBtnText || 'Leave'}
      </Button>,

      <Button
        className={okFirst ? '' : classes.cancelButton}
        onClick={onCancel}>
        {cancelBtnText || 'Stay'}
      </Button>
    ];

    return (
      <Dialog
        open={open}
        onBackdropClick={onCancel}>

        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {children 
              ? <div> {children} </div>
              : <span>{content}</span>
            }
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          {buttons[okFirst ? 1 : 0]}
          {buttons[okFirst ? 0 : 1]}
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles)(ConfirmDialog);
