import React, { PureComponent }  from 'react';
import { connect } from "react-redux";
import { Tabs, Tab } from '@material-ui/core'
import { switchActiveRoom } from '../../actions/translation' 
import {findRoom} from '../../reducers/util/room'

const mapStateToProps = state => {
  return { 
    activeRooms: state.calls.activeRooms,
    currentRoom: state.calls.currentRoom,
    currentRoomId: state.calls.currentRoom ? state.calls.currentRoom.roomUid : -1
  };
};


export class TopBar extends PureComponent {

  handleRoomChange = (e, value) => {
    const room = findRoom({roomUid: value, activeRooms: this.props.activeRooms})
    this.props.switchActiveRoom(room)
  }

  render () {
    const {activeRooms} = this.props;

    if (!activeRooms.length) {
      return (<div>Create or join rooms</div>)

    } else {
      const {currentRoomId} = this.props
      const {handleRoomChange} = this

      return (
        <Tabs onChange={handleRoomChange} value={currentRoomId} >
          {activeRooms.map( room => (
            <Tab value={room.roomUid} key={room.roomUid} label={room.name || 'unnamed'} />
          ))}
        </Tabs>
      )
    }
  }

}


const CTopBar = connect(mapStateToProps, {switchActiveRoom})( TopBar );
export default CTopBar;