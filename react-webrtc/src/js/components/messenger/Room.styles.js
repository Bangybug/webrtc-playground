export default theme => ({
  paper: {
    marginBottom: '16px',
    width: '100%',
    float: 'left'
  },

  newLine: {
    clear: 'both'
  },

  avatar: {
    maxWidth: '24px'
  },

  chip: {
    minWidth: '140px'
  },

  indented: {
    marginTop: '16px', 
    clear: 'both'
  }


})