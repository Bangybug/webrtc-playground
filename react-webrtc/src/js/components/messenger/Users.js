import React, { PureComponent }  from 'react';
import { connect } from "react-redux";
import { onUserClicked } from '../../actions/ui'
import { Toolbar, List, ListItem, ListItemText, ListItemAvatar, Avatar } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import Styles from './Users.styles'
import CreateRoomMenu from './CreateRoomMenu';

const mapStateToProps = state => {
  return { onlineUsers: state.messenger.onlineUsers };
};

export class Users extends PureComponent {

  handleItemClick = (e) => {
    this.props.onUserClicked( this.props.onlineUsers[ Number(e.currentTarget.id) ] )
  }

  render () {
    const {handleItemClick} = this;
    const {classes, onlineUsers} = this.props;
    return (
      <>
        <Toolbar className={classes.topBar}>
          <div className={classes.title}>
            Users
          </div>
          <CreateRoomMenu/>
        </Toolbar>
        <List dense className={classes.root}>
          {onlineUsers.map( (u,i) => (
            <ListItem id={i} key={u.id} button onClick={handleItemClick}>
              <ListItemAvatar>
                <Avatar
                  alt={u.name}
                  src={`${process.env.PUBLIC_URL}/icons/animals/${u.name}.svg`}
                />
              </ListItemAvatar>

              <ListItemText primary={u.name}/>
            </ListItem>
          ))}
        </List>
      </>
      );
  }

}

const CUsers = connect(mapStateToProps, {onUserClicked})( withStyles(Styles)(Users) );
export default CUsers;