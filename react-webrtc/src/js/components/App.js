import React, { Component }  from 'react';
import { connect } from "react-redux";

import theme from '../theme/theme';
import { NoSsr } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';
import {AppBar, Typography} from '@material-ui/core'
import CssBaseline from '@material-ui/core/CssBaseline';
import ConfirmDialog from './messenger/ConfirmDialog'

import Page from './Page';

import './App.css';
import TopBar from './messenger/TopBar'
import Users from './messenger/Users'
import { register, getOnlineUsers } from '../actions/login';
import { startWebsocket } from '../actions/websocket'
import { createRoom, declineCall, joinCall, describeCallMedia } from '../actions/translation'
import Room from './messenger/Room';


const mapStateToProps = state => {
  return { 
    registered: state.auth.registered, 
    me: state.auth.me,
    loggedIn: state.auth.loggedIn,
    currentRoom: state.calls.currentRoom,
    incomingCallInvitation: state.calls.incomingCallInvitation
  };
};


class App extends Component {

  componentDidMount() {
    this.props.register()
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.registered && !prevProps.registered) {
      this.props.startWebsocket(this.props.me.id)
    }
    if (this.props.loggedIn && !prevProps.loggedIn) {
      this.props.getOnlineUsers()

      // TMP autostart room
      this.props.createRoom({p2p:true, media: describeCallMedia({
        allowAudio: true, 
        allowVideo: true,
        allowScreenCapture: true, 
        allowCanvasDraw: true
      })})
    }
  }

  handleCallJoin = () => {
    this.props.joinCall(this.props.me, this.props.incomingCallInvitation)
  }

  handleCallDecline = () => {
    this.props.declineCall(this.props.me, this.props.incomingCallInvitation)
  }

  render() {
    const { incomingCallInvitation } = this.props
    let invitationText
    if (incomingCallInvitation) {
      invitationText = 'Invitation from '+(incomingCallInvitation.invitedBy ? incomingCallInvitation.invitedBy.name : '?unknown?')
    }

    return (
    <>
      <CssBaseline />
      <ThemeProvider theme={theme}>
        <NoSsr>
          <Page
            fullWidth
            Header={() => (
              <AppBar position="static">
                <div className="title">
                  <Typography variant="h6">
                    WebRTC demonstration
                  </Typography>
                </div>
            </AppBar>)}
            TopBar={() => <TopBar/>}
            Navigation={() => <Users/>}>

            {this.props.currentRoom && <Room currentRoom={this.props.currentRoom}/>}

            <ConfirmDialog
              open={Boolean(incomingCallInvitation)}
              title='Incoming call'
              content={invitationText}
              confirmBtnText='Join'
              cancelBtnText='Decline'
              okFirst
              onConfirm={this.handleCallJoin}
              onCancel={this.handleCallDecline} />

          </Page>
        </NoSsr>
      </ThemeProvider>
    </>);
  }
}

const actions = {
  register,
  startWebsocket,
  getOnlineUsers,
  createRoom,
  joinCall,
  declineCall
}
const CApp = connect(mapStateToProps, actions)(App);
export default CApp;