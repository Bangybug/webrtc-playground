import React, { PureComponent } from 'react';
import Styles from './UserMediaStream.styles'
import { connect } from "react-redux";
import { withStyles } from '@material-ui/styles';
import { Chip, CircularProgress, IconButton } from '@material-ui/core'
import { USERSTATE_INVITED, USERSTATE_WAITING } from '../../reducers/util/room';
import { Close } from '@material-ui/icons'
import { kickFromCall } from '../../actions/translation'
import { PreviewVideo, PreviewAudio, streamKey } from './Preview'


// When audio and video is on (within one stream), it's better to use only video element for both audio and video.
// We do not dismount audio/video elements within UserMediaStream lifecycle, becuase it's better to reuse them, so we control visibility with css.

export class UserMediaStream extends PureComponent {

  handleKick = (userId, roomUid) => {
    this.props.kickFromCall(this.props.me, {toUserId:userId, roomUid})
  }


  render () {
    const {user, userState, stream, classes, roomUid} = this.props
    
    const rtcStream = stream && stream.streams && stream.streams[Object.keys(stream.streams)[0]]

    const showCircularProgress = userState.state === USERSTATE_INVITED || userState.state === USERSTATE_WAITING

    return (
      <div className={classes.root}>

        <div className = {classes.actionButton}>
          <IconButton onClick={() => this.handleKick(user.id, roomUid)}>
            <Close />
          </IconButton>
        </div>

        <div className={classes.chipWrapper}>
          <Chip
            className={classes.chip}
            icon={<img className={classes.avatar} alt={user.name} src={`${process.env.PUBLIC_URL}/icons/animals/${user.name}.svg`}/>}
            label={user.name}
          />

          {showCircularProgress && (
            <CircularProgress size={28} className={classes.progress}/>
          )}
        </div>

        <PreviewAudio streamKey={streamKey(stream)} rtcStream={rtcStream}/>

        <div className = {classes.control}>
          {userState.state || 'waiting...'}
        </div>

        <PreviewVideo streamKey={streamKey(stream)} rtcStream={rtcStream}/>

        
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { 
    me: state.auth.me
  };
};

const actions = {
  kickFromCall
}

const CUserMediaStream = connect( mapStateToProps, actions )( withStyles(Styles)(UserMediaStream) );
export default CUserMediaStream;