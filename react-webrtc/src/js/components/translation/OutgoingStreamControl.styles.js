export default theme => ({
  track: {
    display: 'block',
    clear: 'both',
    '& > div': {
      lineHeight: '45px',
      marginRight: '4px',
      float: 'left',
      display: 'inline-block'
    }
  },

  videoPreview: {
    display: 'inline-block',
    float: 'left',
    clear: 'both',
    paddingLeft: 2,
    paddingTop: 2,
    paddingRight: 2,
    maxWidth: 328,
    marginBottom: 16
  },

  video: {
    maxWidth: 320
  },

  videoOff: {
    display: 'none !important'
  }
  
})