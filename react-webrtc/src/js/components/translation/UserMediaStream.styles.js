export default theme => ({
  root: {
    height: '60px',
    display: 'block',
    clear: 'both'
  },
  
  chipWrapper: {
    position: 'relative'
  },

  progress: {
    position: 'absolute',
    top: 16,
    left: 2+60
  },

  yellow: {
    color: 'yellow !important'
  },

  chip: {
    minWidth: '100px',
    float: 'left',
    marginTop: '14px',
    '& > span': {
      flexGrow: 1
    }
  },

  control: {
    marginLeft:'16px',
    float: 'left',
    lineHeight: '60px'
  },

  actionButton: {
    float: 'left',
    lineHeight: '60px',
    marginRight: '12px'
  },

  avatar: {
    maxWidth: '24px'
  }
  
})