export default theme => ({
  video: {
    maxWidth: 320
  },

  videoOff: {
    display: 'none !important'
  },

  videoPreview: {
    display: 'inline-block',
    float: 'left',
    clear: 'both',
    paddingLeft: 2,
    paddingTop: 2,
    paddingRight: 2,
    maxWidth: 328,
    marginBottom: 16
  },


  audioVisible: {
    display: 'block'
  },

  audioInvisible: {
    display: 'none'
  }
  
})