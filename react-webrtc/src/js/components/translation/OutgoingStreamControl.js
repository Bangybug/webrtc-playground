import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { withStyles } from '@material-ui/styles';
import Styles from './OutgoingStreamControl.styles'
import { IconButton } from '@material-ui/core'
import { Videocam, VideocamOff, Mic, MicOff, ScreenShare, StopScreenShare } from '@material-ui/icons'
import { PreviewVideo, streamKey } from './Preview'
import { updateLocalTrackStreamingState, TRACKKIND_AUDIO, TRACKKIND_VIDEO, TRACKKIND_SCREEN } from '../../actions/translation'
import { findLocalStreamAndTrack } from '../../reducers/util/room'

export class OutgoingStreamControl extends PureComponent {
  state = {
    streamKey: streamKey(this.props.stream),
    audioEnabled: false,
    videoEnabled: false,
    screenSharingEnabled: false
  }

  static defaultProps = {
    user: {},
    userState: {},
    stream: {},
    room: null,
    supportedTracks: {}
  }

  componentDidUpdate() {
    const {stream} = this.props
    const currentStreamKey = streamKey(stream)
    //console.log('outgoing stream updated', stream, currentStreamKey)
    if (this.state.streamKey !== currentStreamKey) {
      this.setState({streamKey: currentStreamKey})
    } 
  }


  toggleMute = (trackKind) => {
    // for now lets have max only one track per video or audio in the stream
    // TODO investigate how can multiple video/audio tracks be sent in sync within one stream in peerconnection
    const {updateLocalTrackStreamingState, room} = this.props
    let enabled 
    if (trackKind === TRACKKIND_SCREEN) {
      enabled = !this.state.screenSharingEnabled
      if (enabled && this.state.videoEnabled) {
        updateLocalTrackStreamingState(room.roomUid, TRACKKIND_VIDEO, room.userStreams, false)
      }
      this.setState({screenSharingEnabled: enabled, videoEnabled: enabled ? false : this.state.videoEnabled})
    } else if (trackKind === TRACKKIND_VIDEO) {
      enabled = !this.state.videoEnabled
      if (enabled && this.state.screenSharingEnabled) {
        updateLocalTrackStreamingState(room.roomUid, TRACKKIND_SCREEN, room.userStreams, false)
      }
      this.setState({videoEnabled: enabled, screenSharingEnabled: enabled ? false : this.state.screenSharingEnabled})
    } else if (trackKind === TRACKKIND_AUDIO) {
      enabled = !this.state.audioEnabled
      this.setState({audioEnabled: enabled})
    }
    updateLocalTrackStreamingState(room.roomUid, trackKind, room.userStreams, enabled)
  }

  Track = (props) => {
    const {enabled, offIcon, onIcon, trackKind, label} = props;
    const {classes} = this.props;

    return (
      <div className={classes.track}>
        <div> 
          <IconButton color={enabled ? 'primary' : 'secondary'} onClick={() => this.toggleMute(trackKind)}>
            {enabled ? onIcon : offIcon}
          </IconButton>
        </div>
        <div> 
          Track {trackKind} &nbsp; {label}  
        </div>
      </div>
    )
  }

  render () {
    const { stream, classes, supportedTracks, room } = this.props;
    const { Track } = this;
    const { streamKey, audioEnabled, videoEnabled, screenSharingEnabled } = this.state;

    const {trackStream} = findLocalStreamAndTrack(TRACKKIND_SCREEN, stream) || findLocalStreamAndTrack(TRACKKIND_VIDEO, stream) || {}

    console.log('Stream controls updated', trackStream && trackStream.getTracks())

    return (
      <div className={classes.userControl}>
        {supportedTracks.audio && room.media.allowAudio && (
          <Track trackKind={TRACKKIND_AUDIO} onIcon={<Mic/>} offIcon={<MicOff/>} enabled={audioEnabled} />
        )}
        {supportedTracks.video && room.media.allowVideo && (
          <Track trackKind={TRACKKIND_VIDEO} onIcon={<Videocam/>} offIcon={<VideocamOff/>} enabled={videoEnabled} />
        )}
        {room.media.allowScreenCapture && (
          <Track trackKind={TRACKKIND_SCREEN} onIcon={<ScreenShare/>} offIcon={<StopScreenShare/>} enabled={screenSharingEnabled} />
        )}
        {trackStream && <PreviewVideo streamKey={streamKey} rtcStream={trackStream}/>}
      </div>
    )
  }
}



const mapStateToProps = state => {
  return { 
    me: state.auth.me,
    supportedTracks: state.calls.supportedTracks
  };
};

const actions = {
  updateLocalTrackStreamingState
}

const COutgoingStreamControl = connect( mapStateToProps, actions )( withStyles(Styles)(OutgoingStreamControl) );
export default COutgoingStreamControl;
