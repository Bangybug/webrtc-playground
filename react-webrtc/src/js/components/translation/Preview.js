import React, { PureComponent } from 'react'
import Styles from './Preview.styles'
import { withStyles } from '@material-ui/styles'
import { Card } from '@material-ui/core'
//import { debugStream } from '../../util/rtc'


export const queryStreamTracks = (rtcStream) => {
  return {
    hasAudio: rtcStream && rtcStream.getTracks().some( t => t.kind === 'audio' && t.enabled && !t.muted && !t.ended),
    hasVideo: rtcStream && rtcStream.getTracks().some( t => t.kind === 'video' && t.enabled && !t.muted && !t.ended)
  }
}

// used to trigger component updates
export const streamKey = (stream) => {
  if (stream) {
    function reducer(acc, rtcStream) {
      return acc + rtcStream.getTracks().reduce( ((acc, t) => acc + t.id+'='+(t.enabled && !t.muted && !t.ended)), '' )
    }
    let key = '';
    if (stream.streams) {
      key = Object.keys(stream.streams).map( streamId => stream.streams[streamId] ).reduce(reducer, key)
    }
    if (stream.userMediaStreams) {
      key = Object.keys(stream.userMediaStreams).map( streamId => stream.userMediaStreams[streamId] ).reduce(reducer, key)
    }
    if (stream.displayMediaStreams) {
      key = Object.keys(stream.displayMediaStreams).map( streamId => stream.displayMediaStreams[streamId] ).reduce(reducer, key)
    }
    return key
  }
  return 'stream-muted'
}


class CPreviewVideo extends PureComponent {

  refVideo = null

  componentDidUpdate() {
    if (this.refVideo) {
      const {rtcStream} = this.props
      const caps = queryStreamTracks(rtcStream)
      console.log('preview updated', caps)
      this.refVideo.srcObject = caps.hasVideo ? rtcStream : null
    }
  }

  render() {
    const {rtcStream, classes} = this.props

    const caps = queryStreamTracks(rtcStream)
    const videoClasses = [classes.videoPreview, !caps.hasVideo ? classes.videoOff : ''].join(' ')
    
    return (
      <Card className={videoClasses} variant='outlined'>
        <video 
          playsInline autoPlay 
          className={classes.video}
          ref={video => { 
            this.refVideo = video
          }} 
        />
      </Card>
    )
  }
}

class CPreviewAudio extends PureComponent {

  refAudio = null

  componentDidUpdate() {
    if (this.refAudio) {
      this.refAudio.srcObject = this.props.rtcStream
    }
  }

  render() {
    const {rtcStream, classes} = this.props

    const caps = queryStreamTracks(rtcStream)
    const myClasses = [classes.control,(caps.hasAudio && !caps.hasVideo) ? classes.audioVisible : classes.audioInvisible ].join(' ')

    return (
      <div className={myClasses}>
        <audio 
          controls autoPlay
          ref={audio => { 
            this.refAudio = audio
          }}
        />
      </div>
    )
  }
}


const PreviewVideo = withStyles(Styles)(CPreviewVideo)
const PreviewAudio = withStyles(Styles)(CPreviewAudio)
export { PreviewVideo, PreviewAudio }
