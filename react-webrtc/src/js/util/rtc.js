export function debugStream(rtcStream) {
  console.log('RTC-STREAM DEBUG')
  if (rtcStream) { 
    console.log(rtcStream.id)
    console.log( rtcStream.getTracks().map( t => [t.label, 'enabled='+t.enabled, 'muted='+t.muted, "ended=",t.ended, t]) )
  }
}

export async function startScreenCapture() {
  if (navigator.getDisplayMedia) {
    return navigator.getDisplayMedia({video: true});
  } else if (navigator.mediaDevices.getDisplayMedia) {
    return navigator.mediaDevices.getDisplayMedia({video: true});
  } else {
    return navigator.mediaDevices.getUserMedia({video: {mediaSource: 'screen'}});
  }
}
/**
 * When we just want audio/video, but no capable devices present, we need to remove constraints to avoid errors.
 */
export async function fixConstraints(constraints) {
  if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
    return fallbackFixConstraints(constraints);
  }
  return navigator.mediaDevices.enumerateDevices()
    .then(function(devices) {
      let hasVideoInput = false
      let hasAudioInput = false
      for (const device of devices) {
        //console.log(device.kind + ": " + device.label + " id = " + device.deviceId);
        hasAudioInput = hasAudioInput || device.kind === 'audioinput';
        hasVideoInput = hasVideoInput || device.kind === 'videoinput';
        if (hasAudioInput && hasVideoInput) {
          break;
        }
      }
      if (constraints.audio && !hasAudioInput) {
        delete constraints.audio;
      }
      if (constraints.video && !hasVideoInput) {
        delete constraints.video;
      }
      return constraints;
    })
    .catch(function(err) {
      console.error(err)
      return fallbackFixConstraints(constraints)
    });
}

async function fallbackFixConstraints(constraints) {
  // usually the problem is in video (no webcam)
  return navigator.mediaDevices.getUserMedia(constraints)
    .then(stream => {
      stream.getTracks().forEach( t => t.stop() )
      return constraints;
    })
    .catch(e => {
      if (constraints.video) {
        delete constraints.video;
      }
      return constraints
    })
}


// Copied from AppRTC's sdputils.js:

// Sets |codec| as the default |type| codec if it's present.
// The format of |codec| is 'NAME/RATE', e.g. 'opus/48000'.
export function maybePreferCodec(sdp, type, dir, codec) {
  const str = `${type} ${dir} codec`;
  if (codec === '') {
    console.log(`No preference on ${str}.`);
    return sdp;
  }
  
  console.log(`Prefer ${str}: ${codec}`);

  const sdpLines = sdp.split('\r\n');

  // Search for m line.
  const mLineIndex = findLine(sdpLines, 'm=', type);
  if (mLineIndex === null) {
    return sdp;
  }

  // If the codec is available, set it as the default in m line.
  const codecIndex = findLine(sdpLines, 'a=rtpmap', codec);
  console.log('codecIndex', codecIndex);
  if (codecIndex) {
    const payload = getCodecPayloadType(sdpLines[codecIndex]);
    if (payload) {
      sdpLines[mLineIndex] = setDefaultCodec(sdpLines[mLineIndex], payload);
    }
  }

  sdp = sdpLines.join('\r\n');
  return sdp;
}


// Find the line in sdpLines that starts with |prefix|, and, if specified,
// contains |substr| (case-insensitive search).
export function findLine(sdpLines, prefix, substr) {
  return findLineInRange(sdpLines, 0, -1, prefix, substr);
}

// Find the line in sdpLines[startLine...endLine - 1] that starts with |prefix|
// and, if specified, contains |substr| (case-insensitive search).
export function findLineInRange(sdpLines, startLine, endLine, prefix, substr) {
  const realEndLine = endLine !== -1 ? endLine : sdpLines.length;
  for (let i = startLine; i < realEndLine; ++i) {
    if (sdpLines[i].indexOf(prefix) === 0) {
      if (!substr ||
        sdpLines[i].toLowerCase().indexOf(substr.toLowerCase()) !== -1) {
        return i;
      }
    }
  }
  return null;
}

// Gets the codec payload type from an a=rtpmap:X line.
export function getCodecPayloadType(sdpLine) {
  const pattern = new RegExp('a=rtpmap:(\\d+) \\w+\\/\\d+');
  const result = sdpLine.match(pattern);
  return (result && result.length === 2) ? result[1] : null;
}

// Returns a new m= line with the specified codec as the first one.
export function setDefaultCodec(mLine, payload) {
  const elements = mLine.split(' ');

  // Just copy the first three parameters; codec order starts on fourth.
  const newLine = elements.slice(0, 3);

  // Put target payload first and copy in the rest.
  newLine.push(payload);
  for (let i = 3; i < elements.length; i++) {
    if (elements[i] !== payload) {
      newLine.push(elements[i]);
    }
  }
  return newLine.join(' ');
}