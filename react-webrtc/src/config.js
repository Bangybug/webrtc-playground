export const addr = '10.7.36.29:8081'
//export const addr = 'localhost:8081'

export const urlBackend = 'http://'+addr+'/'
export const wsBackend = 'ws://'+addr+'/v1/ws/'
export const urlServerInfo = urlBackend+'info'
export const urlRegisterUser = urlBackend+'registerUser'
export const urlOnlineUsers = urlBackend+'onlineUsers'

export const rtcServersConfig = {
  iceServers: [{
    urls: 'stun:stun.l.google.com:19302'
  }]
}
