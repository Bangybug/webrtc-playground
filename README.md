### WebRTC POC

This webapp has a ligtweight signalig server that acts like a hub by retranslating every websocket message to all connected clients.

Websocket messages have json structure, all messages typically end up as Redux actions on Frontend where they are executed. Example of such actions are LOGIN, REGISTER, CALL_MAKE, CALL_INVITE, CALL_HANGOUT, etc.

The frontend is written in React/Redux. It allows you to invite connected users to attend a web-conference with WebRTC.


### Run websocket signaling server using Go language:

`cd go-messenger`

`go run main.go`

It will listen on 8081 port. To change it, edit main.go.


### Run Javascript frontend using NPM:

Edit `react-webrtc/src/config.js` where you can set `addr` to point to your machine IP address and port where signaling server listens at. 

Then run:

`cd react-webrtc`

`npm i`

`npm start`

The NPM is used only to host www/js content, it does not have any server logic, so you can use any webserver at your choice if you need to.
The dev server will be accessible via [http://localhost:3000](http://localhost:3000)

You will need to set explicit permissions in your browser to allow it accessing your media resources: https://medium.com/@Carmichaelize/enabling-the-microphone-camera-in-chrome-for-local-unsecure-origins-9c90c3149339  

### Useful links and docs

Quite good overview of Webrtc
https://www.html5rocks.com/en/tutorials/webrtc/basics/

My overview of Webrtc infrastructure
https://docs.google.com/document/d/1U1V_c18SaW2LvVupUa6G8FJenaU4kibx_eySc-dzcps/edit

My dev notes during this poc development
https://docs.google.com/document/d/1-ImML46zWgHLJZUKdVMLNZ6T6kPGSLdoybpSOJyx74o/edit