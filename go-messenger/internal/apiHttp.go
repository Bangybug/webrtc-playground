package internal

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

type ApiHttp struct {
	users           *Users
	serverTimestamp int64
}

func NewApiHttp(serverTimestamp int64, users *Users) *ApiHttp {
	return &ApiHttp{serverTimestamp: serverTimestamp, users: users}
}

func (api *ApiHttp) getOnlineUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(api.users.getOnlineUsers())
}

func (api *ApiHttp) getServerInfo(w http.ResponseWriter, r *http.Request) {
	var jsonBlob = `{"serverTimestamp": "` + strconv.FormatInt(api.serverTimestamp, 16) + `"}`
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, jsonBlob)
}

func (api *ApiHttp) getUserById(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	w.Header().Set("Content-Type", "application/json")
	uid, _ := strconv.ParseInt(params["id"], 10, 32)
	u := api.users.getUserById(int(uid))
	json.NewEncoder(w).Encode(u)
}

func (api *ApiHttp) registerUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	u := api.users.register()
	json.NewEncoder(w).Encode(u)
}

func (api *ApiHttp) AttachHandlers(router *mux.Router) {
	router.HandleFunc("/info", api.getServerInfo).Methods("GET")
	router.HandleFunc("/onlineUsers", api.getOnlineUsers).Methods("GET")
	router.HandleFunc("/user/{id}", api.getUserById).Methods("GET")
	router.HandleFunc("/registerUser", api.registerUser).Methods("GET")
}
