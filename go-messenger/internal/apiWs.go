package internal

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"strconv"
)

type ApiWs struct {
	users           *Users
	serverTimestamp int64

	clients    map[*WsClient]bool
	broadcast  chan []byte
	register   chan *WsClient
	unregister chan *WsClient
}

func NewApiWs(serverTimestamp int64, users *Users) *ApiWs {
	return &ApiWs{
		serverTimestamp: serverTimestamp,
		users:           users,
		register:        make(chan *WsClient),
		unregister:      make(chan *WsClient),
		broadcast:       make(chan []byte),
		clients:         make(map[*WsClient]bool),
	}
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  65536,
	WriteBufferSize: 65536,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

func (api *ApiWs) handleWsConnection(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	uid, _ := strconv.ParseInt(params["id"], 10, 32)
	u := api.users.getUserById(int(uid))
	if u == nil {
		log.Println("Unknown user connected to websocket with id " + params["id"])
		return
	}

	log.Println("Client registered " + params["id"])

	addToOnlineUsers := !api.hasUserOnline(int(uid))
	if addToOnlineUsers {
		api.users.onUserOnline(int(uid))
	}

	client := NewWsClient(api, conn, u)
	api.register <- client

	log.Println("Starting client handlers")

	go client.writePump()
	go client.readPump()
}

func (api *ApiWs) AttachHandlers(router *mux.Router) {
	router.HandleFunc("/v1/ws/{id}", api.handleWsConnection)
}

func (api *ApiWs) hasUserOnline(uid int) bool {
	log.Println("Check if user still online due to multiple connections" + string(uid))
	for client := range api.clients {
		if client.user.Id == uid {
			log.Println("\ttrue")
			return true
		}
	}
	return false
}

func (api *ApiWs) doBroadcast(msg []byte) {
	api.broadcast <- msg
}

func (api *ApiWs) withdraw(client *WsClient) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered from ", r)
		}
	}()

	uid := client.user.Id
	name := client.user.Name

	defer func() {
		if !api.hasUserOnline(uid) {
			api.users.onUserOffline(uid)
			// broadcast needs to be executed in goroutine to avoid deadlock, because withdraw method is called within another goroutine (Run) that owns the lock over api.broadcast channel
			go api.doBroadcast(EncodeUserStatusChange(uid, name, false))
		}
	}()

	log.Println("unreg - delete")
	delete(api.clients, client)
	log.Println("unreg - close")
	close(client.send)
	log.Println("unreg - done")
}

// started outside as go routine
func (api *ApiWs) Run() {
	// For sustainability should have recover with an outgoing way to restart this
	for {
		select {
		case client := <-api.register:
			api.clients[client] = true
		case client := <-api.unregister:
			if _, ok := api.clients[client]; ok {
				api.withdraw(client)
			}
		case message := <-api.broadcast:
			for client := range api.clients {
				select {
				case client.send <- message:
				default:
					api.withdraw(client)
				}
			}
		}
	}
}
