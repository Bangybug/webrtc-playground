package internal

import (
	"encoding/json"
)

type UserStatusChange struct {
	Id     int    `json:"id"`
	Name   string `json:"name"`
	Online bool   `json:"online"`
	Type   string `json:"type"`
}

func NewUserStatusChange(id int, name string, online bool) *UserStatusChange {
	var msgType string
	if !online {
		msgType = "LOGOUT"
	} else {
		msgType = "LOGIN"
	}

	return &UserStatusChange{Id: id, Name: name, Online: online, Type: msgType}
}

func EncodeUserStatusChange(id int, name string, online bool) []byte {
	b, _ := json.Marshal(NewUserStatusChange(id, name, online))
	return b
}
