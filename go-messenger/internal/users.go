package internal

import (
	"math/rand"
	"time"
)

var allNames = [...]string{"ant-eater", "badger", "bear", "bull", "camel", "cat", "chameleon", "cock", "cow-1", "cow", "crocodile", "deer", "dog", "dolphin", "donkey", "duck", "elephant", "fish", "fox", "giraffe", "goat", "goose", "hedgehog", "hippopotamus", "horse", "kangaroo", "koala", "lion", "monkey", "moose", "mouse", "panda", "panther", "penguin", "pig", "rabbit", "racoon", "raven", "rhinoceros", "seal", "sheep", "snail", "snake", "squirrel", "tiger", "turtle", "unicorn", "vulture", "wolf", "zebra"}

type User struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type Users struct {
	nameCount       int
	onlineUsers     []*User
	registeredUsers []User
	lastId          int
}

func NewUsers() *Users {
	return &Users{nameCount: len(allNames), onlineUsers: []*User{}, registeredUsers: []User{}, lastId: 0}
}

func (u *Users) getNewUserName() string {
	if u.nameCount == len(allNames) {
		u.nameCount = -1
		rand.Seed(time.Now().UnixNano())
		rand.Shuffle(len(allNames), func(i, j int) { allNames[i], allNames[j] = allNames[j], allNames[i] })
	}
	u.nameCount++
	return allNames[u.nameCount]
}

func (u *Users) register() *User {
	x := User{Id: u.lastId, Name: u.getNewUserName()}
	u.lastId++
	u.registeredUsers = append(u.registeredUsers, x)
	return &u.registeredUsers[len(u.registeredUsers)-1]
}

func (u *Users) unregister(id int) {
	for i, item := range u.registeredUsers {
		if item.Id == id {
			u.registeredUsers[i] = u.registeredUsers[len(u.registeredUsers)-1]
			u.registeredUsers = u.registeredUsers[:len(u.registeredUsers)-1]
			break
		}
	}
}

func (u *Users) getOnlineUsers() []*User {
	return u.onlineUsers
}

func (u *Users) getUserById(id int) *User {
	for _, item := range u.registeredUsers {
		if item.Id == id {
			return &item
		}
	}
	return nil
}

func (u *Users) onUserOffline(id int) {
	for i, item := range u.onlineUsers {
		if item.Id == id {
			u.onlineUsers[i] = u.onlineUsers[len(u.onlineUsers)-1]
			u.onlineUsers = u.onlineUsers[:len(u.onlineUsers)-1]
			break
		}
	}
}

func (u *Users) onUserOnline(id int) {
	x := u.getUserById(id)
	if u != nil {
		u.onlineUsers = append(u.onlineUsers, x)
	}
}
