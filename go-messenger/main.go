package main

import (
	"./internal"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"time"
)

var backendPort string = ":8081"

func main() {
	r := mux.NewRouter()

	serverTimestamp := time.Now().UnixNano() // every run invalidates all context, give a chance to the clients to know about it
	users := internal.NewUsers()
	apiHttp := internal.NewApiHttp(serverTimestamp, users)
	apiWs := internal.NewApiWs(serverTimestamp, users)

	apiHttp.AttachHandlers(r)
	apiWs.AttachHandlers(r)

	go apiWs.Run()

	println("Running...")
	log.Fatal(http.ListenAndServe(backendPort, handlers.CORS()(r)))
}
